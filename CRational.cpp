// in the name of God

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>
using namespace std;

// in ebarat , har tedad space zade shode ra rad mikonad ( dar tabe'e 
// shenasayiKon be kar borde shode)
#define ebarat_plus_plus while(*ebarat == ' '){ ebarat++;}

//CRational class
class CRational {
public:
	CRational(){}
	CRational(long s, long m):soorat(s),makhraj(m){sadeKon();}

	void setSoorat(long s);
	void setMakhraj(long m);
	long getSoorat();
	long getMakhraj();
	double getAshari();
	void sadeKon();
	CRational operator + (CRational &other);
	CRational operator * (CRational &other);
	CRational operator - (CRational &other);
	CRational operator / (CRational &other);
private:
	long soorat;
	long makhraj;
};

//CList class
class CList {
public:
	CList(string esm, long soorat, long makhraj);
	CList ():sar(this),badi(NULL){} 
	CList *badi;
	CList *sar; 
	
	void ezafeKon(string esm, long soorat, long makhraj);
	void setName(string);
	string getName();
	void setAdad(CRational);
	CRational getAdad();

	CList* peidaKon(string naam);

private:
	string name;
	CRational adadeKasri;
};

// CRational Definitions
CRational CRational::operator+(CRational &other)
{
	return CRational(soorat * other.makhraj + other.soorat * makhraj  , makhraj * other.makhraj);
}
CRational CRational::operator-(CRational &other)
{
	return CRational(soorat * other.makhraj - other.soorat * makhraj  , makhraj * other.makhraj);
}
CRational CRational::operator*(CRational &other)
{
	return CRational(soorat * other.soorat , makhraj * other.makhraj);
}
CRational CRational::operator/(CRational &other)
{
	return CRational(soorat * other.makhraj , makhraj * other.soorat);
}

void CRational::sadeKon(){
	long a, b, c=1;
	a = soorat;
	b = makhraj;
	if(a > b)
	{
		c = a;
		a = b;
		b = c;
	}

	while(1){
		c = b % a;
		if ( c == 0)
			break;
		b = a;
		a = c;
	}

	soorat /= a;
	makhraj /= a;
}


long CRational::getSoorat(){
	return soorat;
}

long CRational::getMakhraj(){
	return makhraj;
}

void CRational::setSoorat(long s){
	soorat = s;
}

void CRational::setMakhraj(long m){
	makhraj = m;
}

double CRational::getAshari(){
	return (double) soorat / makhraj;
}


//CList Definitions
CList::CList(string esm, long soorat, long makhraj) : sar(this),badi(NULL){
	name = esm;
	adadeKasri.setSoorat(soorat);
	adadeKasri.setMakhraj(makhraj);
}

void CList::setAdad(CRational adad){ adadeKasri = adad ;}
CRational CList::getAdad(){return adadeKasri; }
string CList::getName(){ return name; }
void CList::setName(string esm) { name = esm; }

void CList::ezafeKon(string esm, long soorat, long makhraj)
{
	if(!peidaKon(esm)){
		CList *temp;
		CList *head = new CList(esm,soorat,makhraj);
		temp = sar->badi;
		sar->badi = head;
		sar->badi->badi = temp;
	}
}

CList* CList::peidaKon(string naam){
	CList *head = sar;

	while(head)
	{
		if(head->getName() == naam)
			return head;
		head = head->badi;
	}
	return NULL;
}
// end of classes definitions

// Program globals
CList *myList = NULL;

//Program function prototypes
void def(string name, string soorat, string makhraj);
void add(string first, string second, string third);
void mult(string first, string second, string third);
void sub(string first, string second, string third);
void div(string first, string second, string third);
void end();
void print(string esm);
void printd(string esm);
void shenasayiKon(string farman); // yek farman ra shenasayi va ejra mikonad

//Function definitions
void def(string name, string soorat, string makhraj)
{
	const char* a = soorat.data();
	const char* b = makhraj.data();

	if(myList == NULL)
		myList = new CList(name, atol(a), atol(b));
	else	// atol , string ra be long tabdil mikonad
		myList->ezafeKon(name, atol(a), atol(b));
}

void add(string first, string second, string third)
{
	CList *avali = myList->peidaKon(first);
	CList *dovomi = myList->peidaKon(second);
	CList *sevomi = myList->peidaKon(third);

	if(avali == NULL || dovomi == NULL || sevomi == NULL)
		return;
	CRational a = dovomi->getAdad();
	CRational b = sevomi->getAdad();
	CRational c = a + b;
	avali->setAdad(c);
}

void mult(string first, string second, string third)
{
	CList *avali = myList->peidaKon(first);
	CList *dovomi = myList->peidaKon(second);
	CList *sevomi = myList->peidaKon(third);

	if(avali == NULL || dovomi == NULL || sevomi == NULL)
		return;
	CRational a = dovomi->getAdad();
	CRational b = sevomi->getAdad();
	CRational c = a * b;
	avali->setAdad(c);
}

void sub(string first, string second, string third)
{
	CList *avali = myList->peidaKon(first);
	CList *dovomi = myList->peidaKon(second);
	CList *sevomi = myList->peidaKon(third);

	if(avali == NULL || dovomi == NULL || sevomi == NULL)
		return;
	CRational a = dovomi->getAdad();
	CRational b = sevomi->getAdad();
	CRational c = a - b;
	avali->setAdad(c);
}

void div(string first, string second, string third)
{
	CList *avali = myList->peidaKon(first);
	CList *dovomi = myList->peidaKon(second);
	CList *sevomi = myList->peidaKon(third);

	if(avali == NULL || dovomi == NULL || sevomi == NULL)
		return;
	CRational a = dovomi->getAdad();
	CRational b = sevomi->getAdad();
	CRational c = a / b;
	avali->setAdad(c);
}

void print(string esm)
{
	CList *ozv = myList->peidaKon(esm);
	if(ozv == NULL) 
		return;
	CRational adad = ozv->getAdad();
	cout << adad.getSoorat() << "/" << adad.getMakhraj()<<endl;
}

void printd(string esm)
{
	CList *ozv = myList->peidaKon(esm);
	if(ozv==NULL)
		return;
	cout << ozv->getAdad().getAshari() <<endl;
}

void end(){exit(0);}

void shenasayiKon(string farman)
{
	const char *ebarat = farman.data();
	string command; // dastoor(mesle def , add, mult, ...)
	string first; // avalin argument
	string second; // dovomin argument
	string third; // sevomin argument
	
	while(*ebarat != ' ' && *ebarat != '\0')
	{
		command += *ebarat;
		ebarat++;
	}

	if(command == "end")
		end();
	
	ebarat_plus_plus
	while(*ebarat != ' ' && *ebarat != '\0')
	{
		first += *ebarat;
		ebarat ++;
	}
	if(command == "print"){
		print(first);
		return;
	}
	if(command == "printd"){
		printd(first);
		return;
	}

	ebarat_plus_plus
	while(*ebarat != ' ' && *ebarat != '\0'){
		second += *ebarat;
		ebarat++;
	}
	
	ebarat_plus_plus
	while(*ebarat != ' ' && *ebarat != '\0'){
		third += *ebarat;
		ebarat++;
	}
	
	if(command == "def"){
		def(first,second, third);
		return;
	}
	if(command == "add"){
		add(first,second,third);
		return;
	}
	if(command == "mult"){
		mult(first,second,third);
		return;
	}
	if(command == "sub"){
		sub(first,second,third);
		return;
	}
	if(command == "div"){
		div(first,second,third);
		return;
	}
}
//az rooye file mikhanad. esme file  in.txt bayad bashad
//main function
void main()
{
	FILE *myFile;
	string farman;
	char matn='1';
	string mymatn;
	const char * mymatn2;
	myFile = fopen("in.txt","r");

	if (!myFile)
	{
		cout <<"sharmande Baz nashod"<<endl;
		exit(0);
	}
	
	while((fscanf(myFile, "%c" ,&matn))>0){
		mymatn += matn;
	}
	fclose(myFile);
	mymatn2 = mymatn.data();
	while(1){
		while(*mymatn2 != '\n')
		{
			farman += *mymatn2;
			mymatn2++;
		}
		mymatn2++;
		shenasayiKon(farman);
		farman ="";
	}
}